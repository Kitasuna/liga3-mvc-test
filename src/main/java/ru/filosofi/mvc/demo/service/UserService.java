package ru.filosofi.mvc.demo.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.filosofi.mvc.demo.model.User;
import ru.filosofi.mvc.demo.repo.UserRepo;
import ru.filosofi.mvc.demo.util.validator.UserValidator;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.UUID;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class UserService {

    private final UserRepo userRepo;
    private final UserValidator userValidator;

    @Transactional
    public String save(User user) {
        String message;
        if (userValidator.emailIsValid(user)) {
            userRepo.save(user);
            message = "Saved!";
        } else {
            message = "Invalid email! " + user.getEmail();
        }
        return  message;
    }

    public List<User> findAll() {
        return userRepo.findAll();
    }

    public User findOne(Long id) {
        return userRepo.findById(id)
                .orElseThrow(() -> new EntityNotFoundException());
    }
}
