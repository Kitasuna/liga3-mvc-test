package ru.filosofi.mvc.demo.exception;

public class UserInvalid extends RuntimeException{
    public UserInvalid(String msg) {
        super(msg);
    }
}
