package ru.filosofi.mvc.demo.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.filosofi.mvc.demo.model.User;
import ru.filosofi.mvc.demo.service.UserService;

import java.util.List;
import java.util.Random;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping("/{id}")
    public User getOne(@PathVariable ("id") Long id) {
        return userService.findOne(id);
    }

    @PostMapping("/save")
    public String save(@RequestBody User user) {
        user.setId(new Random().nextLong());
        return userService.save(user);
    }

    @GetMapping
    public List<User> findAll() {
        return userService.findAll();
    }
}
