package ru.filosofi.mvc.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.filosofi.mvc.demo.model.User;

@Repository
public interface UserRepo extends JpaRepository<User, Long> {
}
