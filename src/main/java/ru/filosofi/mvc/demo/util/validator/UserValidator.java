package ru.filosofi.mvc.demo.util.validator;

import org.springframework.stereotype.Component;
import ru.filosofi.mvc.demo.exception.UserInvalid;
import ru.filosofi.mvc.demo.model.User;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class UserValidator {

    private static final String regex = "^(.+)@(.+)$";


    public boolean emailIsValid(User user) {
        String email = user.getEmail();
        if (Objects.isNull(email)) {
            throw new UserInvalid("Email can't be null!");
        }
        return Pattern.compile(regex)
                      .matcher(email)
                      .matches();
    }
}
